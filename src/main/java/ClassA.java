public class ClassA {

    public int getFinalValue() throws Exception {
        ClassB classB = FactoryClass.getObject();
        int value = classB.getValue();
        if(value==5) {
            throw new Exception("sample exception");
        }
        int newValue = classB.getNewValue(value);
        return newValue + 5;
    }
}
