import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.testng.PowerMockTestCase;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.powermock.api.mockito.PowerMockito.*;
import static org.testng.Assert.assertEquals;

@PrepareForTest({FactoryClass.class})
public class ClassATest extends PowerMockTestCase {

    private ClassB classB;

    @BeforeMethod
    public void setUp() throws Exception {

        mockStatic(FactoryClass.class);
        classB = mock(ClassB.class);
        when(FactoryClass.class, "getObject").thenReturn(classB);
        when(classB.getValue()).thenReturn(6);
        when(classB.getNewValue(any(Integer.class))).thenReturn(10);
    }

    @Test
    public void test() throws Exception {
        ClassA classA = new ClassA();
        int newValue = classA.getFinalValue();
        assertEquals(newValue,15);
    }

    @Test(expectedExceptions = Exception.class)
    public void testException() throws Exception {
        when(classB.getValue()).thenReturn(5);
        ClassA classA = new ClassA();
        classA.getFinalValue();
    }
}
